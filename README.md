# Airbnb-Decentral

## Setup the project
yarn

## Run the project
yarn start

## Setup Moralis Server
In index.js to specify appId and serverUrl of MoralisProvider

## Connect with the smart contract
Use Remix to compile and deploy the smart contract.
Change the Rentals.js to change the contractAddress to the newly deployed smart contract's address

## Google map api
Change apiKey in RentalsMap.js to have correct google map api key (chargeable)

## Moralis setup of data sync on rentalCreated events
New Rental Created
rentalCreated (string, string,string,string,string,string,string,uint256,uint256,string[],uint256 address)

  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "string",
        "name": "name",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "city",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "lat",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "long",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "unoDescription",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "dosDescription",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "string",
        "name": "imgUrl",
        "type": "string"
      },
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "maxGuests",
        "type": "uint256"
      },
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "pricePerDay",
        "type": "uint256"
      },
      {
        "indexed": false,
        "internalType": "string[]",
        "name": "datesBooked",
        "type": "string[]"
      },
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "id",
        "type": "uint256"
      },
      {
        "indexed": false,
        "internalType": "address",
        "name": "renter",
        "type": "address"
      }
    ],
    "name": "rentalCreated",
    "type": "event"
  }

0x7AbE23a2BE7aa53192882BA22aA1E1085d9015D1

Rentals


