[
    {
      attributes: {
        city: "New York",
        unoDescription: "3 Guests • 2 Beds • 2 Rooms",
        dosDescription: "Wifi • Kitchen • Living Area",
        imgUrl:
          "https://ipfs.moralis.io:2053/ipfs/QmS3gdXVcjM72JSGH82ZEvu4D7nS6sYhbi5YyCw8u8z4pE/media/3",
        lat: "40.716862",
        long: "-73.999005",
        name: "Apartment in China Town",
        pricePerDay: "3",
      },
    },
    {
      attributes: {
        city: "New York",
        unoDescription: "1 Guests • 1 Bed",
        dosDescription: "Wifi • Self Checkin • Work Station",
        imgUrl:
          "https://ipfs.moralis.io:2053/ipfs/QmS3gdXVcjM72JSGH82ZEvu4D7nS6sYhbi5YyCw8u8z4pE/media/2",
        lat: "40.767589",
        long: "-73.969231",
        name: "Studio in Central Park",
        pricePerDay: "1",
      },
    },
    {
      attributes: {
        city: "London",
        unoDescription: "2 Guests • 1 Bed • 1 Bath",
        dosDescription: "Wifi • Kitchen • Living Area",
        imgUrl:
          "https://ipfs.moralis.io:2053/ipfs/QmS3gdXVcjM72JSGH82ZEvu4D7nS6sYhbi5YyCw8u8z4pE/media/7",
        lat: "51.500075",
        long: "-0.125706",
        name: "Tropical Ambiance in London",
        pricePerDay: "1",
      },
    },
  ];
  